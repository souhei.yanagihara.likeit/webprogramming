
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.userDao;
import model.User;

/**
 * Servlet implementation class UserCreate
 */
@WebServlet("/UserCreate")
public class UserCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/NewUser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//リクエストパラメータの取得
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordCon = request.getParameter("passwordCon");
		String name = request.getParameter("name");
		String birthDay = request.getParameter("birthDay");

		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao UserDao = new userDao();

		//エラーメッセージ
		if (!password.equals(passwordCon)) {
			request.setAttribute("errMsg", "パスワードとパスワード(確認)の内容が異なります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/NewUser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//エラーメッセージ
		ArrayList<userDao> list = new ArrayList<userDao>();
		if (loginId.length() == 0 || password.length() == 0 || passwordCon.length() == 0
				|| name.length() == 0 || birthDay.length() == 0) {
			request.setAttribute("errMsg", "未入力な欄があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/NewUser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//エラーメッセージ
		User user = UserDao.CheackLoginInfo(loginId);
		if (user != null) {
			request.setAttribute("errMsg", "このログインIDは既に登録されています");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/NewUser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserDao.insert(loginId, password, name, birthDay);

		//ユーザー一覧にリダイレクト
		response.sendRedirect("UserListServlet");
		return;
	}

}
