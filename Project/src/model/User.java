package model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDay;
	private String updateDay;

	public User() {
	}

	public User(String password) {
		this.password = password;
	}

	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;

	}

	public User(String loginId, String name, int id) {
		this.loginId = loginId;
		this.name = name;
		this.id = id;
	}

	public User(int id, String loginId, String name, Date birthDate, String password, String createDay,
			String updateDay) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDay = createDay;
		this.updateDay = updateDay;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDay() {
		return createDay;
	}

	public void setCreateDay(String createDay) {
		this.createDay = createDay;
	}

	public String getUpdateDay() {
		return updateDay;
	}

	public void setUpdatwDay(String updateDay) {
		this.updateDay = updateDay;
	}
}
