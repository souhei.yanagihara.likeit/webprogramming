
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.userDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao UserDao = new userDao();
		User user = UserDao.CheackUser(id);

		//リクエストスコープにインスタンスを保存
		request.setAttribute("userUpdate", user);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//リクエストパラメータの入力項目を取得
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDay = request.getParameter("birthDay");
		String password = request.getParameter("password");
		String passwordCon = request.getParameter("passwordCon");

		//Daoのメソッドを実行
		userDao UserDao = new userDao();

		//パスワードとパスワード(確認)の内容が異なる場合
		if (!password.equals(passwordCon)) {
			request.setAttribute("errMsg", "入力された内容が正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		ArrayList<userDao> list = new ArrayList<userDao>();
		//パスワード以外で未入力な欄がある場合
		if (name.length() == 0 || birthDay.length() == 0) {
			request.setAttribute("errMsg", "入力されていない欄があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワードとパスワード(確認)がどちらとも空欄だった場合
		if (password.length() == 0 && passwordCon.length() == 0) {
			UserDao.upDate(id, loginId, name, birthDay);
		} else {
			//入力がすべて正しく行えた場合
			UserDao.allUpdate(id, loginId, password, name, birthDay);
		}

		//ユーザー一覧にリダイレクト
		response.sendRedirect("UserListServlet");
		return;

	}
}
