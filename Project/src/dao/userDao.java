package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class userDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection con = null;
		try {
			con = DBmanager.getConnection();

			String sql = "SELECT*FROM user WHERE login_id=? and password=?";

			String encryptionPass = encryption(password);
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, encryptionPass);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			int userId = rs.getInt("id");
			return new User(loginIdDate, nameDate, userId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
	}

	public List<User> findAll() {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {
			con = DBmanager.getConnection();

			String sql = "SELECT*FROM user WHERE id != 1";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDay = rs.getString("create_date");
				String updateDay = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDay, updateDay);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public List<User> findSearch(String loginIdP, String nameP, String startDate, String endDate) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {
			con = DBmanager.getConnection();

			String sql = "SELECT*FROM user WHERE id != 1 ";

			if (!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "'";
			}

			if (!nameP.equals("")) {
				sql += " AND name LIKE  '%" + nameP + "%'";
			}

			if (!startDate.equals("")) {
				sql += " AND birth_date >='" + startDate + "'";
			}

			if (!endDate.equals("")) {
				sql += " AND birth_date <='" + endDate + "'";
			}

			System.out.println(sql);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDay = rs.getString("create_date");
				String updateDay = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDay, updateDay);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void insert(String loginId, String password, String name, String birthDay) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBmanager.getConnection();

			String InsertSQL = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,NOW(),NOW())";
			pStmt = con.prepareStatement(InsertSQL);

			//パスワード暗号化
			String encryptionPass = encryption(password);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDay);
			pStmt.setString(4, encryptionPass);

			int result = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User CheackLoginInfo(String loginId) {
		Connection con = null;
		try {
			con = DBmanager.getConnection();

			String sql = "SELECT*FROM user WHERE login_id=? ";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			return new User(loginIdDate, nameDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
	}

	public User CheackUser(String id) {
		Connection con = null;
		try {
			con = DBmanager.getConnection();

			String sql = "SELECT*FROM user WHERE id=? ";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int idDate = rs.getInt("id");
			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			Date birthDayDate = rs.getDate("birth_date");
			String passwordDate = rs.getString("password");
			String createDay = rs.getString("create_date");
			String updateDay = rs.getString("update_date");

			return new User(idDate, loginIdDate, nameDate, birthDayDate, passwordDate, createDay, updateDay);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
	}

	public void delete(String Id) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBmanager.getConnection();

			String DeleteSQL = "DELETE FROM user WHERE id=?";
			pStmt = con.prepareStatement(DeleteSQL);

			pStmt.setString(1, Id);

			int result = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void allUpdate(String id, String loginId, String password, String name, String birthDay) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBmanager.getConnection();

			String DeleteSQL = "update user set login_id=?,name=?,birth_date=?,password=? where id=? ";
			pStmt = con.prepareStatement(DeleteSQL);

			//パスワード暗号化
			String encryptionPass = encryption(password);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDay);
			pStmt.setString(4, encryptionPass);
			pStmt.setString(5, id);

			int result = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void upDate(String id, String loginId, String name, String birthDay) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBmanager.getConnection();

			String UpdateSQL = "update user set login_id=?,name=?,birth_date=? where id=?";
			pStmt = con.prepareStatement(UpdateSQL);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDay);
			pStmt.setString(4, id);

			int result = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//暗号化メソッド
	public String encryption(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		//標準出力
		return result;

	}

}
