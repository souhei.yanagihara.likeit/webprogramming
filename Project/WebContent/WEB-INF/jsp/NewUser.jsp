<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="col-6 mx-auto">

			<h1>
				<p>ユーザー新規登録</p>
			</h1>

			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<form class="form-signin" action="UserCreate" method="post">
				<div class="row">
					<div class="col-5">ログインID</div>
					<div class="col-sm-5">
						<input type="text" name="loginId" syle="width: 200px;"
							class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-5">パスワード</div>
					<div class="col-sm-7">
						<input type="text" name="password" style="width: 200px;"
							class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-5">パスワード(確認)</div>
					<div class="col-sm-6">
						<input type="text" name="passwordCon" style="width: 200px;"
							class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-5">ユーザー名</div>
					<div class="col-sm-6">
						<input type="text" name="name" style="width: 200px;"
							class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-5">生年月日</div>
					<div class="col-sm-6">
						<input type="text" name="birthDay" style="width: 200px;"
							class="form-control">
					</div>
				</div>

				<p>
					<input type="hidden" name="createDay" value="createDay">登録日時
				</p>

				<p>
					<input type="hidden" name="updateDay" value="updateDay">更新日時
				</p>

				<div class="row">
					<div class="col-6 mx-auto">
						<input type="submit" value="登録">
					</div>
				</div>
			</form>

			<p></p>
			<p></p>
			<a href="UserListServlet"><u><p>
						<font color=#0000FF>戻る</font>
					</p></u></a>
		</div>
	</div>

</body>
</html>


