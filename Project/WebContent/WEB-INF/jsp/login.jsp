<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col-8 mx-auto">
				<h1>
					<p>ログイン画面</p>
				</h1>

				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>

				<form class="form-signin" action="LoginServlet" method="post">
					<div class="row">
						<div class="col-5">ログインID</div>
						<div class="col-sm-7">
							<input type="text" name="loginId" style="width: 200px;"
								class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-5">パスワード</div>
						<div class="col-sm-6">
							<input type="text" name="password" style="width: 200px;"
								class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-6 mx-auto">
							<button type="submit">ログイン</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>


