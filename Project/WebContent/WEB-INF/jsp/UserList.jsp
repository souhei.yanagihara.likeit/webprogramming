<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col-10 mx-auto">

				<header>
					<div align="right">
						<h5>
							<li>${userInfo.name}さんログイン中</li>
							<li><a href="UserLogoutServlet ">ログアウト</a></li>
						</h5>
					</div>
				</header>

				<h1>
					<p>ユーザー一覧</p>
				</h1>

				<div align="right">
					<h5>
						<p>
							<a href="UserCreate"><font color=#0000FF>新規登録</font></a>
						</p>
					</h5>
				</div>

				<form class="form-signin" action="UserListServlet" method="post">
					<div class="row">
						<div class="col-5">ログインID</div>
						<div class="col-sm-7">
							<input type="text" name="loginId" style="width: 200px;"
								class="form-control">
						</div>
					</div>

					<div class="row">
						<div class="col-5">ユーザー名</div>
						<div class="col-sm-6">
							<input type="text" name="name" style="width: 200px;"
								class="form-control">
						</div>
					</div>

					<div class="row">
						<div class="col-5">生年月日</div>
						<div class="col-sm-6">
							<input type="date" name="startDate">
							<p>~</p>
							<input type="date" name="endDate">

						</div>
					</div>


					<div class="row">
						<div class="col- mx-auto">
							<button type="submit">検索</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<hr>

		<table class="table">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>ユーザー名</th>
					<th>生年月日</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>

						<c:choose>
							<c:when test="${userInfo.id==1}">
								<td><a href="UserDetailServlet?id=${user.id }">詳細</a></td>
								<td><a href="UserUpdateServlet?id=${user.id }">更新</a></td>
								<td><a href="UserDeleteServlet?id=${user.id }">削除</a></td>
							</c:when>
							<c:otherwise>
								<td><a href="UserDetailServlet?id=${user.id }">詳細</a></td>
								<c:if test="${userInfo.id== user.id}">
									<td><a href="UserUpdateServlet?id=${user.id }">更新</a></td>
								</c:if>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>



