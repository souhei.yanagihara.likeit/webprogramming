<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="col-6 mx-auto">

			<h1>
				<p>ユーザー情報更新</p>
			</h1>

			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<form action="UserUpdateServlet" method="post">
			<div class="row">
				<div class="col-5">ログインID</div>
				<div class="col-sm-5">
					<p>${userUpdate.loginId}</p>
					<p>
						<input type="hidden" name="loginId" value="${userUpdate.loginId}">
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-5">パスワード</div>
				<div class="col-sm-7">
					<input type="password" name="password" style="width: 200px;"
						class="form-control">
				</div>
			</div>

			<div class="row">
				<div class="col-5">パスワード(確認)</div>
				<div class="col-sm-6">
					<input type="password" name="passwordCon" style="width: 200px;"
						class="form-control">
				</div>
			</div>

			<div class="row">
				<div class="col-5">ユーザー名</div>
				<div class="col-sm-5">
					<input type="text" name="name" value="${userUpdate.name }"
						style="width: 200px;" class="form-control">
				</div>
			</div>

			<div class="row">
				<div class="col-5">生年月日</div>
				<div class="col-sm-6">
					<input type="date" name="birthDay"
						value="${userUpdate. birthDate }" style="width: 200px;"
						class="form-control">
				</div>
			</div>


				<div class="row">
					<div class="col-6 mx-auto">
						<input type="hidden" name="id" value="${userUpdate.id}">
						<button type="submit">更新</button>
					</div>
				</div>
			</form>

			<p></p>

			<a href="UserListServlet"><u><p>
						<font color=#0000FF>戻る</font>
					</p></u></a>

		</div>
	</div>

</body>
</html>


