
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col-10 mx-auto">
				<h1>
					<p>ユーザー情報詳細参照</p>
				</h1>
			</div>
			<form>
				<div class="row">
					<div class="col-5">ログインID</div>
					<div class="col-sm-5"></div>
					<p>${userDetail.loginId }</p>
				</div>

				<div class="row">
					<div class="col-5">ユーザー名</div>
					<div class="col-sm-5"></div>
					<p>${userDetail.name}</p>
				</div>

				<div class="row">
					<div class="col-5">生年月日</div>
					<div class="col-sm-5"></div>
					<p>${userDetail. birthDate}</p>
				</div>

				<div class="row">
					<div class="col-5">登録日時</div>
					<div class="col-sm-3"></div>
					<p>${userDetail.createDay }</p>
				</div>

				<div class="row">
					<div class="col-5">更新日時</div>
					<div class="col-sm-3"></div>
					<p>${userDetail.updateDay }</p>
				</div>
			</form>


			<p></p>
			<a href="UserListServlet"><u><p>
						<font color=#0000FF>戻る</font>
					</p></u></a>
		</div>
	</div>

</body>
</html>


