
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="col-6 mx-auto">
			<div class="col-8 mx-auto">
				<h1>ユーザー削除確認</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<p>
			ログインID:${userDelete.loginId}<br>
			を本当に削除してよろしいでしょうか
		</p>
	</div>

	<div>
		<div class="container">
		<form action="UserDeleteServlet" method="post">
			<div class="col-6 mx-auto">
				<div class="col-8 mx-auto">
					<a href="UserListServlet">
					<input type="button" name="done" value="キャンセル" id="submit_button"></a>

							<input type="hidden" name="id" value="${userDelete.id}">
							<input type="submit" name="to_correct" value="OK" id="submit_button">

				</div>
			</div>
			</form>
		</div>
	</div>
</body>
</html>
